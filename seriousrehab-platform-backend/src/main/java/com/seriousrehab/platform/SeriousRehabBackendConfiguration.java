package com.seriousrehab.platform;

import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties({SeriousRehabKeycloakProperties.class})
public class SeriousRehabBackendConfiguration {

    private final SeriousRehabKeycloakProperties keycloakProperties;

    @Bean
    public RealmResource keycloak() {
        //TODO: make secure?
        return Keycloak.getInstance(
                keycloakProperties.getUrl(),
                keycloakProperties.getRealm(),
                keycloakProperties.getUsername(),
                keycloakProperties.getPassword(),
                keycloakProperties.getClientid()).realm(keycloakProperties.getRealm());
    }

}
