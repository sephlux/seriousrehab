package com.seriousrehab.platform;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Map;


@Slf4j
public class IpFilter extends OncePerRequestFilter {

    private final Map<String, ZonedDateTime> blockedIpAddresses;

    public IpFilter(Map<String, ZonedDateTime> blockedIpAddresses) {
        this.blockedIpAddresses = blockedIpAddresses;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String ipAddress = request.getRemoteAddr();
        log.debug("ip address: {}", ipAddress);
        if (blockedIpAddresses.containsKey(ipAddress)) {
            ZonedDateTime blockedUntil = blockedIpAddresses.get(ipAddress);
            if (blockedUntil.isAfter(ZonedDateTime.now(blockedUntil.getZone()))) {
                log.info("detected blocked ip address {}. requests will be blocked until {}", ipAddress, blockedUntil);
                response.sendError(HttpServletResponse.SC_PAYMENT_REQUIRED);
            } else {
                log.info("ip address block has expired at {}. future requests will not be blocked", blockedUntil);
                filterChain.doFilter(request, response);
            }
        } else {
            log.debug("ip address not blocked");
            filterChain.doFilter(request, response);
        }
    }
}
