package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.model.GameSession;
import com.seriousrehab.platform.model.SaveGame;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.GameSessionNotFoundException;
import com.seriousrehab.platform.repository.GameSessionRepository;
import com.seriousrehab.platform.repository.SaveGameRepository;
import com.seriousrehab.platform.service.SaveGameService;
import com.seriousrehab.platform.service.UserService;
import com.seriousrehab.slack.SimpleNotificationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.json.JsonObject;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class SaveGameServiceImpl implements SaveGameService {

    private final SaveGameRepository saveGameRepository;

    private final GameSessionRepository gameSessionRepository;

    private final ApplicationEventPublisher eventPublisher;

    private final UserService userService;

    @Override
    public Optional<GameSession> getGameSession(String id) {
        return gameSessionRepository.findById(id)
                .map(this::loadSavedGames);
    }

    @Override
    public Optional<GameSession> getGameSession(String id, String userId) {
        return gameSessionRepository.findByIdAndUserId(id, userId)
                .map(this::loadSavedGames);
    }

    @Override
    public Optional<GameSession> getGameSessionByTherapist(String id, String therapistId) {
        return gameSessionRepository.findByIdAndTherapistId(id, therapistId)
                .map(this::loadSavedGames);
    }

    @Override
    public List<GameSession> getAllGameSessionsByUserId(String userId) {
        return gameSessionRepository.findAllByUserIdOrderByCreatedAtDesc(userId)
                .stream()
                .map(this::loadSavedGames)
                .toList();
    }

    @Override
    public List<GameSession> getAllGameSessionsByUserIdAndTherapistId(String userId, String therapistId) {
        return gameSessionRepository.findAllByUserIdAndTherapistIdOrderByCreatedAtDesc(userId, therapistId)
                .stream()
                .map(this::loadSavedGames)
                .toList();
    }

    @Override
    public List<GameSession> getAllGameSessionsFromTherapist(String therapistId) {
        return gameSessionRepository.findAllByTherapistIdOrderByCreatedAtDesc(therapistId)
                .stream()
                .map(this::loadSavedGames)
                .toList();
    }

    @Override
    public List<GameSession> getAllGameSessions() {
        return gameSessionRepository.findAllByOrderByCreatedAtDesc()
                .stream()
                .map(this::loadSavedGames)
                .toList();
    }

    @Override
    public void syncGames(SaveGame saveGame) {
        log.info("payload {}", saveGame.getPayload());
        eventPublisher.publishEvent(new SimpleNotificationEvent("syncGames " + saveGame));
        BsonDocument document = new JsonObject(saveGame.getPayload()).toBsonDocument();
        document.getArray("playedGames")
                .stream()
                .map(BsonValue::asDocument)
                .forEach(bsonDocument -> {
                    String id = bsonDocument.getString("id").getValue();
                    Optional<SaveGame> tmp = saveGameRepository.findByUnityId(id);
                    SaveGame target;
                    if (tmp.isEmpty()) {
                        target = saveGameRepository.save(SaveGame.builder()
                                .unityId(id)
                                .gameId(saveGame.getGameId())
                                .userId(saveGame.getUserId())
                                .payload(bsonDocument.toJson())
                                .build());
                        log.info("save game synced: {}", bsonDocument.toJson());
                    } else {
                        target = saveGameRepository.findByUnityId(id).get();
                        log.info("save game already synced: {}", bsonDocument.getString("id").getValue());
                    }

//                    List<GameSession> gameSessions = gameSessionRepository.findAllByUserIdAndGameIdAndCreatedAtBetween(saveGame.getUserId(), saveGame.getGameId(),
//                            saveGame.getCreatedAt().toLocalDate().atStartOfDay(),
//                            saveGame.getCreatedAt().toLocalDate().atTime(23, 59, 59));
                    List<GameSession> gameSessions = gameSessionRepository.findAllByUserIdAndGameIdAndCreatedAtBetween(target.getUserId(), target.getGameId(),
                            target.getCreatedAt().toLocalDate().atStartOfDay(),
                            target.getCreatedAt().toLocalDate().atTime(LocalTime.MAX));
                    if (gameSessions.isEmpty()) {
                        String therapistId = userService.findUserById(target.getUserId())
                                .map(User::getTherapistId)
                                .orElse(target.getUserId());
                        GameSession gameSession = GameSession.builder()
                                .userId(target.getUserId())
                                .gameId(target.getGameId())
                                .therapistId(therapistId)
                                .build();
                        gameSessionRepository.save(gameSession);
                    } else if (gameSessions.size() > 1) {
                        // should not happen
                        log.error("The gameSessions size per day should not be above one!");
                    }

                });
    }

    @Override
    public GameSession updateGameSession(String sessionId, GameSession gameSession) {
        GameSession target = gameSessionRepository.findById(sessionId)
                .orElseThrow(GameSessionNotFoundException::new);

        target.setPain(gameSession.getPain());
        target.setSwelling(gameSession.getSwelling());
        target.setFeeling(gameSession.getFeeling());
        target.setPainComment(gameSession.getPainComment());
        target.setSwellingComment(gameSession.getSwellingComment());
        target.setFeelingComment(gameSession.getFeelingComment());
        return gameSessionRepository.save(target);
    }

    private GameSession loadSavedGames(GameSession gameSession) {
        gameSession.setSaveGames(
//                saveGameRepository
//                        .findAllByUserIdAndGameIdAndCreatedAtBetweenOrderByCreatedAtDesc(gameSession.getUserId(), gameSession.getGameId(),
//                                gameSession.getCreatedAt().toLocalDate().atStartOfDay(),
//                                gameSession.getCreatedAt().toLocalDate().atTime(23, 59, 59))
                saveGameRepository
                        .findAllByUserIdAndGameIdAndCreatedAtBetweenOrderByCreatedAtDesc(gameSession.getUserId(), gameSession.getGameId(),
                                gameSession.getCreatedAt().toLocalDate().atStartOfDay(),
                                gameSession.getCreatedAt().toLocalDate().atTime(LocalTime.MAX))
        );
        return gameSession;
    }
}
