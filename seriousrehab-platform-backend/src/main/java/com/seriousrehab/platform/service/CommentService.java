package com.seriousrehab.platform.service;

import com.seriousrehab.platform.model.Comment;

import java.util.List;

public interface CommentService {

    Comment createComment(Comment comment);

    List<Comment> findAllComments(String targetId);

}
