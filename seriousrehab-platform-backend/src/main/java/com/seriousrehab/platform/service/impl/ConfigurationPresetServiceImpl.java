package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.model.ConfigurationPreset;
import com.seriousrehab.platform.repository.ConfigurationPresetRepository;
import com.seriousrehab.platform.service.ConfigurationPresetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class ConfigurationPresetServiceImpl implements ConfigurationPresetService {

    private final ConfigurationPresetRepository configurationPresetRepository;

    @Override
    public Optional<ConfigurationPreset> findConfigurationPreset(String id, String therapistId) {
        return configurationPresetRepository.findByIdAndTherapistId(id, therapistId);
    }

    @Override
    public Optional<ConfigurationPreset> findConfigurationPreset(String id) {
        return configurationPresetRepository.findById(id);
    }

    @Override
    public List<ConfigurationPreset> findAllConfigurationPreset(String therapistId, String gameId) {
        return configurationPresetRepository.findAllByTherapistIdAndGameId(therapistId, gameId);
    }

    @Override
    public ConfigurationPreset createOrUpdateConfigurationPreset(ConfigurationPreset configurationPreset, String therapistId) {
        Optional<ConfigurationPreset> targetOptional = configurationPresetRepository.findByNameAndTherapistId(configurationPreset.getName(), therapistId);

        ConfigurationPreset target = ConfigurationPreset.builder().build();
        if (targetOptional.isPresent()) {
            target = targetOptional.get();
            target.setModifiedAt(LocalDateTime.now());
            target.setConfiguration(configurationPreset.getConfiguration());
        } else {
            target.setCreatedAt(LocalDateTime.now());
            target.setModifiedAt(LocalDateTime.now());
            target.setGameId(configurationPreset.getGameId());
            target.setTherapistId(therapistId);
            target.setConfiguration(configurationPreset.getConfiguration());
            target.setName(configurationPreset.getName());
        }

        return configurationPresetRepository.save(target);
    }

}
