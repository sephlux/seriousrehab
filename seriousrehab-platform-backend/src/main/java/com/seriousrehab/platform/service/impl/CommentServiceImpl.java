package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.model.Comment;
import com.seriousrehab.platform.repository.CommentRepository;
import com.seriousrehab.platform.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Override
    public Comment createComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public List<Comment> findAllComments(String targetId) {
        return commentRepository.findAllByTargetIdOrderByCreatedAtDesc(targetId);
    }
}
