package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.service.KeycloakApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class KeycloakApiServiceImpl implements KeycloakApiService {

    private final RealmResource realmResource;

//    @Override
//    public List<UserRepresentation> findAllUsers() {
//        return realmResource
//                .users()
//                .list();
//    }
//
//    @Override
//    public List<UserRepresentation> findAllUsersByTherapist(String therapistId) {
//        return realmResource
//                .users()
//                .searchByAttributes("");
//    }

    @Override
    public String createUser(UserRepresentation user) {
        log.info("Creating user with username={}", user.getEmail());
        try (Response response = realmResource.users().create(user)) {
            String id = CreatedResponseUtil.getCreatedId(response);
            log.info("Created user with username={} and id={}", user.getEmail(), id);
            return id;
        }
    }

    @Override
    public void resendInvitationEmail(UserRepresentation user) {
        log.info("resendInvitationEmail {}", user);
//        realmResource.users().get(user.getId()).executeActionsEmail(List.of("UPDATE_PASSWORD"));
//        realmResource.users().get(user.getId()).resetPassword(user.getCredentials().get(0));
        realmResource.users().get(user.getId()).update(user);
    }

    @Override
    public void addRoleToUser(String userId, String clientUuid, RoleRepresentation role) {
        log.info("Adding role={} to user={}", role.getName(), userId);
        realmResource.users().get(userId).roles().clientLevel(clientUuid).add(List.of(role));
    }

    @Override
    public void removeRoleFromUser(String userId, String clientUuid, RoleRepresentation therapistRoleRepresentation) {
        log.info("Removing role={} from user={}", therapistRoleRepresentation.getName(), userId);
        realmResource.users().get(userId).roles().clientLevel(clientUuid).add(List.of(therapistRoleRepresentation));
    }

    public Optional<ClientRepresentation> findClientByClientId(String clientId) {
        return Optional.ofNullable(realmResource.clients().findByClientId(clientId).get(0));
    }

    @Override
    public Optional<RoleRepresentation> findRoleByName(String clientUuid, String roleName) {
        return Optional.ofNullable(realmResource.clients().get(clientUuid).roles().get(roleName).toRepresentation());
    }

    @Override
    public void deleteUser(String userId) {
        log.info("Deleting user={}", userId);
        realmResource.users().get(userId).logout();
        try (Response response = realmResource.users().delete(userId)) {
            log.info("Deleted user={}", response.readEntity(String.class));
        }
    }
}
