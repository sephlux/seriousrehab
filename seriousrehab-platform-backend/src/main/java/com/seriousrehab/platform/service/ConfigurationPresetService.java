package com.seriousrehab.platform.service;

import com.seriousrehab.platform.model.ConfigurationPreset;

import java.util.List;
import java.util.Optional;

public interface ConfigurationPresetService {
    Optional<ConfigurationPreset> findConfigurationPreset(String id, String therapistId);

    Optional<ConfigurationPreset> findConfigurationPreset(String id);

    List<ConfigurationPreset> findAllConfigurationPreset(String therapistId, String gameId);

    ConfigurationPreset createOrUpdateConfigurationPreset(ConfigurationPreset configurationPreset, String therapistId);
}
