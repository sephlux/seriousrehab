package com.seriousrehab.platform.service;

import com.seriousrehab.platform.model.Game;

import java.util.List;
import java.util.Optional;

public interface GameService {

    Game createGame(Game game);

    List<Game> findAllGames();

    List<Game> findAllGamesWithGameType(Game.GameType gameType);

    Game updateGame(String gameId, Game game);

    void deleteGame(String gameId);

    Optional<Game> findGameById(String id);
}
