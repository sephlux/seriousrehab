package com.seriousrehab.platform.service;

import com.seriousrehab.platform.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> findUserByUsername(String username);

    Optional<User> findUserById(String userId);

    Optional<User> findUserByIdAndTherapistId(String userId, String therapistId);

    List<User> findAllUsers();

    List<User> findAllUsersByTherapist(String therapistId);

    User createUser(User user);

    User updateUser(User user);

    User createTherapistUser(User user);

    void deleteUser(String id);

    void resendInvitationEmail(String id);
}
