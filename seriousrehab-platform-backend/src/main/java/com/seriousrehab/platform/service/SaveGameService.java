package com.seriousrehab.platform.service;

import com.seriousrehab.platform.model.GameSession;
import com.seriousrehab.platform.model.SaveGame;

import java.util.List;
import java.util.Optional;

public interface SaveGameService {

    List<GameSession> getAllGameSessionsByUserId(String userId);

    List<GameSession> getAllGameSessionsByUserIdAndTherapistId(String userId, String therapistId);

    List<GameSession> getAllGameSessionsFromTherapist(String therapistId);

    List<GameSession> getAllGameSessions();

    Optional<GameSession> getGameSession(String id);

    Optional<GameSession> getGameSession(String id, String userId);

    Optional<GameSession> getGameSessionByTherapist(String id, String therapistId);

    void syncGames(SaveGame saveGame);

    GameSession updateGameSession(String sessionId, GameSession gameSession);
}
