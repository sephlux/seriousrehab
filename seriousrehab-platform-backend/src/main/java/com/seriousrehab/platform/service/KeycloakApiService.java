package com.seriousrehab.platform.service;

import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.Optional;

public interface KeycloakApiService {

//    List<UserRepresentation> findAllUsers();
//
//    List<UserRepresentation> findAllUsersByTherapist(String therapistId);

    String createUser(UserRepresentation user);

    void resendInvitationEmail(UserRepresentation user);

    void addRoleToUser(String userId, String clientUuid, RoleRepresentation role);

    Optional<ClientRepresentation> findClientByClientId(String clientId);

    Optional<RoleRepresentation> findRoleByName(String clientId, String roleName);

    void deleteUser(String userId);

    void removeRoleFromUser(String userId, String clientUuid, RoleRepresentation therapistRoleRepresentation);
}
