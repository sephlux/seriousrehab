package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.model.Game;
import com.seriousrehab.platform.model.exeptions.GameNotFoundException;
import com.seriousrehab.platform.repository.GameRepository;
import com.seriousrehab.platform.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    @Override
    public Optional<Game> findGameById(String id) {
        return gameRepository.findById(id);
    }

    @Override
    public Game createGame(Game game) {
        log.info("createGame {}", game);
        return gameRepository.save(game);
    }

    @Override
    public List<Game> findAllGames() {
        return gameRepository.findAllByOrderByCreatedAt();
    }

    @Override
    public List<Game> findAllGamesWithGameType(Game.GameType gameType) {
        return gameRepository.findAllByGameTypesContaining(gameType);
    }

    @Override
    public Game updateGame(String gameId, Game game) {
        Game oldGame = gameRepository.findById(gameId)
                .orElseThrow(GameNotFoundException::new);
        oldGame.setGameTags(game.getGameTags());
        oldGame.setGameTypes(game.getGameTypes());
        oldGame.setDescription(game.getDescription());
        oldGame.setStorageUrl(game.getStorageUrl());
        oldGame.setTitle(game.getTitle());
        oldGame.setFileTitle(game.getFileTitle());
        oldGame.setRatio(game.getRatio());
        oldGame.setAngle(game.getAngle());
        oldGame.setConfigurationParameters(game.getConfigurationParameters());
        oldGame.setThumbnailUrl(game.getThumbnailUrl());
        return gameRepository.save(oldGame);
    }

    @Override
    public void deleteGame(String gameId) {

    }
}
