package com.seriousrehab.platform.service.impl;

import com.seriousrehab.platform.SeriousRehabKeycloakProperties;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.EmailAlreadyRegisteredException;
import com.seriousrehab.platform.model.exeptions.KeycloakClientNotFoundException;
import com.seriousrehab.platform.model.exeptions.KeycloakRoleNotFoundException;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.repository.UserRepository;
import com.seriousrehab.platform.service.KeycloakApiService;
import com.seriousrehab.platform.service.UserService;
import com.seriousrehab.slack.SimpleNotificationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final KeycloakApiService keycloakApiService;
    private final ApplicationEventPublisher eventPublisher;
    private final SeriousRehabKeycloakProperties keycloakProperties;

    @Override
    public Optional<User> findUserById(String userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> findUserByIdAndTherapistId(String userId, String therapistId) {
        return userRepository.findByIdAndTherapistId(userId, therapistId);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    //TODO: only admin
    public List<User> findAllUsers() {
//        List<User> users = keycloakApiService.findAllUsers()
//                .stream()
//                .map(userRepresentation ->
//                        User.builder()
//                                .keycloakId(userRepresentation.getId())
//                                .username(userRepresentation.getEmail())
//                                .firstName(userRepresentation.getFirstName())
//                                .lastName(userRepresentation.getLastName())
//                                .build()
//                ).toList();
        return userRepository.findAllByOrderByCreatedAtDesc();
    }

    @Override
    public List<User> findAllUsersByTherapist(String therapistId) {
//        List<User> users = keycloakApiService.findAllUsersByTherapist(therapistId)
//                .stream()
//                .map(userRepresentation ->
//                        User.builder()
//                                .keycloakId(userRepresentation.getId())
//                                .username(userRepresentation.getEmail())
//                                .firstName(userRepresentation.getFirstName())
//                                .lastName(userRepresentation.getLastName())
//                                .build()
//                ).toList();
        return userRepository.findAllByTherapistIdOrderByCreatedAtDesc(therapistId);
    }

    @Override
    public User createUser(User user) {
        log.info("createUser {}", user);
        eventPublisher.publishEvent(new SimpleNotificationEvent("createUser " + user.toString()));
        userRepository.findByUsername(user.getUsername()).ifPresent(presentUser -> {
            log.error("user with the given email already exists {}", user);
            throw new EmailAlreadyRegisteredException();
        });

        UserRepresentation newUser = new UserRepresentation();
        newUser.setEmail(user.getUsername());
        newUser.setEmailVerified(true);
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setUsername(user.getUsername());
        newUser.setEnabled(true);
//        if (!StringUtils.isEmpty(user.getTherapistId())) {
//            newUser.setAttributes(Map.of("therapistId", List.of(user.getTherapistId())));
//        }
        newUser.setEmailVerified(true);
        String pw = generateRandomCharacters(12);
        newUser.setAttributes(Map.of("pw", List.of(pw)));

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(pw);
        credentialRepresentation.setTemporary(true);


        newUser.setCredentials(List.of(credentialRepresentation));
        String userId = keycloakApiService.createUser(newUser);

//        String clientUuid = keycloakApiService.findClientByClientId("seriousrehab-app")
//                .orElseThrow(KeycloakClientNotFoundException::new)
//                .getId();
//        RoleRepresentation userRoleRepresentation = keycloakApiService.findRoleByName(clientUuid, "USER")
//                .orElseThrow(KeycloakRoleNotFoundException::new);
//        RoleRepresentation therapistRoleRepresentation = keycloakApiService.findRoleByName(clientUuid, "THERAPIST")
//                .orElseThrow(KeycloakRoleNotFoundException::new);
//        keycloakApiService.addRoleToUser(userId, clientUuid, userRoleRepresentation);
//        keycloakApiService.removeRoleFromUser(userId, clientUuid, therapistRoleRepresentation);

        user.setKeycloakId(userId);
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        log.info("updateUser {}", user);
        User target = userRepository.findById(user.getId())
                .orElseThrow(UserNotFoundException::new);

        target.setGameSettings(user.getGameSettings());

        return userRepository.save(target);
    }

    @Override
    public User createTherapistUser(User user) {
        log.info("createTherapistUser {}", user);
        eventPublisher.publishEvent(new SimpleNotificationEvent("createTherapistUser " + user.toString()));
        userRepository.findByUsername(user.getUsername()).ifPresent(presentUser -> {
            if (presentUser.isDeleted()) {
                //TODO: reactivate user?
                log.error("user with the given email already exists {}", user);
                throw new EmailAlreadyRegisteredException();
            } else {
                log.error("user with the given email already exists {}", user);
                throw new EmailAlreadyRegisteredException();
            }
        });
        String clientUuid = keycloakApiService.findClientByClientId(keycloakProperties.getClientid())
                .orElseThrow(KeycloakClientNotFoundException::new)
                .getId();
        RoleRepresentation userRoleRepresentation = keycloakApiService.findRoleByName(clientUuid, "THERAPIST")
                .orElseThrow(KeycloakRoleNotFoundException::new);
        keycloakApiService.addRoleToUser(user.getKeycloakId(), clientUuid, userRoleRepresentation);
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String id) {
        log.info("deleteUser {}", id);
        User user = userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
        keycloakApiService.deleteUser(user.getKeycloakId());
        user.setDeleted(true);
        userRepository.save(user);
    }

    @Override
    public void resendInvitationEmail(String id) {
        log.info("resendInvitationEmail {}", id);
        eventPublisher.publishEvent(new SimpleNotificationEvent("resendInvitationEmail " + id));
        User user = userRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
        String pw = generateRandomCharacters(12);
        UserRepresentation updateUser = new UserRepresentation();
        updateUser.setId(user.getKeycloakId());
        updateUser.setAttributes(Map.of("pw", List.of(pw)));
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(pw);
        credentialRepresentation.setTemporary(true);
        updateUser.setCredentials(List.of(credentialRepresentation));

        keycloakApiService.resendInvitationEmail(updateUser);
    }

    public String generateRandomCharacters(int length) {
        RandomStringGenerator pwdGenerator = new RandomStringGenerator.Builder().withinRange(97, 122)
                .build();
        return pwdGenerator.generate(length);
    }
}
