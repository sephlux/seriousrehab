package com.seriousrehab.platform.api.delegate.therapist;

import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.ConfigurationPresetApiDelegate;
import com.seriousrehab.platform.api.mapper.ConfigurationPresetMapper;
import com.seriousrehab.platform.api.model.ConfigurationPresetDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.ConfigurationPresetNotFoundException;
import com.seriousrehab.platform.service.ConfigurationPresetService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TherapistConfigurationPresetApiDelegateImpl implements ConfigurationPresetApiDelegate {

    private final ConfigurationPresetService configurationPresetService;

    private final ApiUserService apiUserService;
    private final ConfigurationPresetMapper configurationPresetMapper;

    @Override
    public ResponseEntity<ConfigurationPresetDto> createOrUpdateConfigurationPreset(ConfigurationPresetDto configurationPresetDto) {
        User currentUser = apiUserService.getUser();
        return ResponseEntity.ok(configurationPresetMapper.toDto(
                configurationPresetService.createOrUpdateConfigurationPreset(configurationPresetMapper.fromDto(configurationPresetDto), currentUser.getId())
        ));
    }

    @Override
    public ResponseEntity<Void> deleteConfigurationPreset(String id) {
        return ConfigurationPresetApiDelegate.super.deleteConfigurationPreset(id);
    }

    @Override
    public ResponseEntity<List<ConfigurationPresetDto>> getAllConfigurationPresets(String gameId) {
        User currentUser = apiUserService.getUser();
        return ResponseEntity.ok(configurationPresetService.findAllConfigurationPreset(currentUser.getId(), gameId).stream()
                .map(configurationPresetMapper::toDto)
                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<ConfigurationPresetDto> getConfigurationById(String id) {
        User currentUser = apiUserService.getUser();
        return configurationPresetService.findConfigurationPreset(id, currentUser.getId())
                .map(configurationPresetMapper::toDto)
                .map(ResponseEntity::ok)
                .orElseThrow(ConfigurationPresetNotFoundException::new);
    }

}
