package com.seriousrehab.platform.api.delegate.therapist;

import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.GamesApiDelegate;
import com.seriousrehab.platform.api.mapper.GameMapper;
import com.seriousrehab.platform.api.model.GameDto;
import com.seriousrehab.platform.model.exeptions.GameNotFoundException;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TherapistGamesApiDelegateImpl implements GamesApiDelegate {

    //    https://www.baeldung.com/rest-api-spring-oauth2-angular
    //    https://www.baeldung.com/spring-security-method-security
    private final GameService gameService;
    private final GameMapper gameMapper;

    private final ApiUserService apiUserService;


    @Override
    public ResponseEntity<GameDto> getGameById(String id) {
        log.info("getGameById {}", id);
        return ResponseEntity.ok(
                gameMapper.toDto(
                        gameService.findGameById(id)
                                .orElseThrow(GameNotFoundException::new)
                )
        );
    }

    @Override
    public ResponseEntity<GameDto> createGame(GameDto gameDto) {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<Void> deleteGame(String id) {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<List<GameDto>> getAllGames() {
        log.info("getAllGames");
        return ResponseEntity.ok(gameService.findAllGames()
                .stream()
                .map(gameMapper::toDto)
                .toList());

//        User user = apiUserService.getUser();
//        return ResponseEntity.ok(gameService.findAllGames()
//                .stream()
//                .filter(game -> user.getGameSettings() == null || (user.getGameSettings() != null && user.getGameSettings().stream()
//                        .filter(gameSetting -> StringUtils.equals(gameSetting.getGameId(), game.getId()))
//                        .filter(User.GameSetting::isEnabled)
//                        .toList().size() == 1))
//                .map(gameMapper::toDto)
//                .toList());
    }

    @Override
    public ResponseEntity<GameDto> updateGame(String id, GameDto gameDto) {
        throw new UnauthorizedMethodCallException();
    }
}
