package com.seriousrehab.platform.api.delegate.admin;

import com.seriousrehab.platform.api.GamesApiDelegate;
import com.seriousrehab.platform.api.mapper.GameMapper;
import com.seriousrehab.platform.api.model.GameDto;
import com.seriousrehab.platform.model.exeptions.GameNotFoundException;
import com.seriousrehab.platform.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminGamesApiDelegateImpl implements GamesApiDelegate {

    //    https://www.baeldung.com/rest-api-spring-oauth2-angular
//    https://www.baeldung.com/spring-security-method-security
    private final GameService gameService;
    private final GameMapper gameMapper;


    @Override
    public ResponseEntity<GameDto> getGameById(String id) {
        log.info("getGameById {}", id);
        return ResponseEntity.ok(
                gameMapper.toDto(
                        gameService.findGameById(id)
                                .orElseThrow(GameNotFoundException::new)
                )
        );
    }

    @Override
    public ResponseEntity<GameDto> createGame(GameDto gameDto) {
        log.info("createGame {}", gameDto);
//        try {
        return ResponseEntity.ok(
                gameMapper.toDto(
                        gameService.createGame(gameMapper.fromDto(gameDto)))
        );
//        } catch (Exception e) {
//            log.error("error", e);
//            throw new ApiErrorException(HttpStatus.BAD_REQUEST, new ApiError().localizedMessage("Unknown Error: " + e.getMessage()));
//        }
    }

    @Override
    public ResponseEntity<Void> deleteGame(String id) {
        log.info("deleteGame {}", id);
//        try {
        gameService.deleteGame(id);
        return ResponseEntity.ok().build();
//        } catch (Exception e) {
//            throw new ApiErrorException(HttpStatus.BAD_REQUEST, new ApiError().localizedMessage("Unknown Error: " + e.getMessage()));
//        }
    }

    @Override
    public ResponseEntity<List<GameDto>> getAllGames() {
        log.info("getAllGames");
        return ResponseEntity.ok(gameService.findAllGames()
                .stream()
                .map(gameMapper::toDto)
                .toList());

    }

    @Override
    public ResponseEntity<GameDto> updateGame(String id, GameDto gameDto) {
        log.info("updateGame {}, {}", id, gameDto);
        return ResponseEntity.ok(
                gameMapper.toDto(
                        gameService.updateGame(id, gameMapper.fromDto(gameDto)))
        );
    }
}
