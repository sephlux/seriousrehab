package com.seriousrehab.platform.api;

import com.seriousrehab.platform.api.model.ApiError;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class ApiErrorException extends RuntimeException {
    HttpStatus httpStatus;
    ApiError apiError;
}
