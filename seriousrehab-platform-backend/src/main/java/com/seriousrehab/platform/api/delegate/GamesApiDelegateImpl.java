package com.seriousrehab.platform.api.delegate;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.GamesApiDelegate;
import com.seriousrehab.platform.api.delegate.admin.AdminGamesApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.therapist.TherapistGamesApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.user.UserGamesApiDelegateImpl;
import com.seriousrehab.platform.api.mapper.GameMapper;
import com.seriousrehab.platform.api.model.GameDto;
import com.seriousrehab.platform.model.exeptions.GameNotFoundException;
import com.seriousrehab.platform.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class GamesApiDelegateImpl implements GamesApiDelegate {

    //    https://www.baeldung.com/rest-api-spring-oauth2-angular
//    https://www.baeldung.com/spring-security-method-security
    private final GameService gameService;
    private final GameMapper gameMapper;
    private final ApiUserService apiUserService;
    private final List<GamesApiDelegate> gamesApiDelegates;

    private GamesApiDelegate getDelegate() {
        ApiUser authenticatedUser = apiUserService.getApiUser();
        for (GamesApiDelegate gamesApiDelegate : gamesApiDelegates) {
            if (authenticatedUser.isAdmin()) {
                if (gamesApiDelegate instanceof AdminGamesApiDelegateImpl) {
                    return gamesApiDelegate;
                }
            } else if (authenticatedUser.isTherapist()) {
                if (gamesApiDelegate instanceof TherapistGamesApiDelegateImpl) {
                    return gamesApiDelegate;
                }
            } else {
                if (gamesApiDelegate instanceof UserGamesApiDelegateImpl) {
                    return gamesApiDelegate;
                }
            }
        }
        throw new IllegalStateException("missing delegate");
    }

    @Override
    public ResponseEntity<GameDto> getGameById(String id) {
        log.info("getGameById {}", id);
        return ResponseEntity.ok(
                gameMapper.toDto(
                        gameService.findGameById(id)
                                .orElseThrow(GameNotFoundException::new)
                )
        );
    }

    @Override
    public ResponseEntity<GameDto> createGame(GameDto gameDto) {
        return getDelegate().createGame(gameDto);
    }

    @Override
    public ResponseEntity<Void> deleteGame(String id) {
        return getDelegate().deleteGame(id);
    }

    @Override
    public ResponseEntity<List<GameDto>> getAllGames() {
        return getDelegate().getAllGames();
    }

    @Override
    public ResponseEntity<GameDto> updateGame(String id, GameDto gameDto) {
        return getDelegate().updateGame(id, gameDto);
    }
}
