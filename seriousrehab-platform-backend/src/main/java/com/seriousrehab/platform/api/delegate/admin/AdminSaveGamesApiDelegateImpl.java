package com.seriousrehab.platform.api.delegate.admin;

import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.SaveGamesApiDelegate;
import com.seriousrehab.platform.api.mapper.GameSessionMapper;
import com.seriousrehab.platform.api.mapper.SaveGameMapper;
import com.seriousrehab.platform.api.model.GameSessionDto;
import com.seriousrehab.platform.api.model.SaveGameDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.SaveGameNotFoundException;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.service.SaveGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminSaveGamesApiDelegateImpl implements SaveGamesApiDelegate {

    private final ApiUserService apiUserService;
    private final SaveGameService saveGameService;
    private final SaveGameMapper saveGameMapper;
    private final GameSessionMapper gameSessionMapper;

    @Override
    public ResponseEntity<List<GameSessionDto>> getAllGameSessions(String userId) {
        log.info("getAllGameSessions {}", userId);

        if (userId != null) {
            return ResponseEntity.ok(saveGameService.getAllGameSessionsByUserId(userId)
                    .stream()
                    .map(gameSessionMapper::toDto)
                    .toList());
        }
        return ResponseEntity.ok(saveGameService.getAllGameSessions()
                .stream()
                .map(gameSessionMapper::toDto)
                .toList());
    }

    @Override
    public ResponseEntity<GameSessionDto> getGameSession(String id) {
        log.info("getGameSession {}", id);
        return ResponseEntity.ok(gameSessionMapper.toDto(
                saveGameService.getGameSession(id)
                        .orElseThrow(SaveGameNotFoundException::new))
        );
    }

    @Override
    public ResponseEntity<Void> syncSaveGames(SaveGameDto saveGameDto) {
        log.info("syncSaveGames {}", saveGameDto);
        User authenticatedUser = apiUserService.getUser();
        saveGameService.syncGames(saveGameMapper.fromDto(saveGameDto, authenticatedUser.getId()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updateGameSession(String id, GameSessionDto gameSessionDto) {
        throw new UnauthorizedMethodCallException();
    }

}
