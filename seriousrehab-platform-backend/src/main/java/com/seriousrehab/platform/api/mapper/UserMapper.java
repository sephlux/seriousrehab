package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.UserDto;
import com.seriousrehab.platform.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class UserMapper implements Mapper<User, UserDto> {

    private final GameSettingMapper gameSettingMapper;

    @Override
    public UserDto toDto(User user) {
        return new UserDto()
                .id(user.getId())
                .lastName(user.getLastName())
                .firstName(user.getFirstName())
                .username(user.getUsername())
                .gameSettings(user.getGameSettings() != null ?
                        user.getGameSettings().stream().map(gameSettingMapper::toDto).toList() :
                        new ArrayList<>())
                .therapistId(user.getTherapistId())
                .createdAt(user.getCreatedAt().atOffset(ZoneOffset.UTC));
    }

    @Override
    public User fromDto(UserDto userDto) {
        return User.builder()
                .lastName(userDto.getLastName())
                .firstName(userDto.getFirstName())
                .username(userDto.getUsername())
                .gameSettings(userDto.getGameSettings() != null ?
                        userDto.getGameSettings().stream().map(gameSettingMapper::fromDto).toList() :
                        new ArrayList<>())
                .therapistId(userDto.getTherapistId())
                .build();
    }
}
