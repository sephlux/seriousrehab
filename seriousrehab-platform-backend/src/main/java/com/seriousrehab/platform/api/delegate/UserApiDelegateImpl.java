package com.seriousrehab.platform.api.delegate;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.UsersApiDelegate;
import com.seriousrehab.platform.api.delegate.admin.AdminUserApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.therapist.TherapistUserApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.user.UserUserApiDelegateImpl;
import com.seriousrehab.platform.api.mapper.UserMapper;
import com.seriousrehab.platform.api.model.KeycloakUserDto;
import com.seriousrehab.platform.api.model.UserDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class UserApiDelegateImpl implements UsersApiDelegate {

    private final UserService userService;
    private final ApiUserService apiUserService;
    private final UserMapper userMapper;

    private final List<UsersApiDelegate> usersApiDelegates;

    private UsersApiDelegate getDelegate() {
        ApiUser authenticatedUser = apiUserService.getApiUser();
        for (UsersApiDelegate usersApiDelegate : usersApiDelegates) {
            if (authenticatedUser.isAdmin()) {
                if (usersApiDelegate instanceof AdminUserApiDelegateImpl) {
                    return usersApiDelegate;
                }
            } else if (authenticatedUser.isTherapist()) {
                if (usersApiDelegate instanceof TherapistUserApiDelegateImpl) {
                    return usersApiDelegate;
                }
            } else {
                if (usersApiDelegate instanceof UserUserApiDelegateImpl) {
                    return usersApiDelegate;
                }
            }
        }
        throw new IllegalStateException("missing delegate");
    }

    //https://stackoverflow.com/questions/43222769/how-to-create-keycloak-client-role-programmatically-and-assign-to-user
    @Override
    public ResponseEntity<UserDto> createUser(UserDto userDto) {
        return getDelegate().createUser(userDto);
    }

    @Override
    public ResponseEntity<Void> deleteUser(String id) {
        return getDelegate().deleteUser(id);
    }

    @Override
    public ResponseEntity<UserDto> getUserById(String id) {
        return getDelegate().getUserById(id);
    }

    @Override
    public ResponseEntity<UserDto> getUserByUsername() {
        // get the user for the user who sent the request
        ApiUser currentUser = apiUserService.getApiUser();
        return ResponseEntity.ok(userService.findUserByUsername(currentUser.getUsername())
                .map(userMapper::toDto)
                .orElseThrow(UserNotFoundException::new));
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return getDelegate().getAllUsers();
    }

    @Override
    public ResponseEntity<UserDto> updateUser(String id, UserDto userDto) {
        return getDelegate().updateUser(id, userDto);
    }

    @Override
    public ResponseEntity<Boolean> handleKeycloakRegistrationEvent(KeycloakUserDto keycloakUserDto) {
        log.info("handleKeycloakRegistrationEvent {}", keycloakUserDto);

        userService.createTherapistUser(User.builder()
                .keycloakId(keycloakUserDto.getKeycloakId())
                .username(keycloakUserDto.getUsername())
                .firstName(keycloakUserDto.getFirstName())
                .lastName(keycloakUserDto.getLastName())
                .therapistId(null)
                .build());

        return ResponseEntity.ok(true);
    }

    @Override
    public ResponseEntity<Void> resendInvitationEmail(String id) {
        ApiUser currentUser = apiUserService.getApiUser();
        if (currentUser.isAdmin() || currentUser.isTherapist()) {
            userService.resendInvitationEmail(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
