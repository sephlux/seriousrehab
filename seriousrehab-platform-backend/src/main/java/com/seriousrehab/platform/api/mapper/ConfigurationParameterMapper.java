package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.ConfigurationParameterDto;
import com.seriousrehab.platform.model.Game;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationParameterMapper implements Mapper<Game.ConfigurationParameter, ConfigurationParameterDto> {
    @Override
    public ConfigurationParameterDto toDto(Game.ConfigurationParameter config) {
        return new ConfigurationParameterDto()
                .fieldName(config.getFieldName())
                .type(config.getType() != null ? config.getType().getType() : null)
                .title(config.getTitle())
                .min(config.getMin())
                .configurable(config.isConfigurable())
                .value(config.getValue())
                .defaultValue(config.getDefaultValue())
                .max(config.getMax());
    }

    public Game.ConfigurationParameter fromDto(ConfigurationParameterDto configDto) {
        Game.ConfigurationParameter config = new Game.ConfigurationParameter();
        config.setFieldName(configDto.getFieldName());
        config.setType(EnumUtils.getEnum(Game.ParameterType.class, configDto.getType().toUpperCase()));
        config.setTitle(configDto.getTitle());
        config.setMin(configDto.getMin());
        config.setConfigurable(configDto.getConfigurable());
        config.setMax(configDto.getMax());
        config.setValue(configDto.getValue());
        config.setDefaultValue(configDto.getDefaultValue());
        return config;
    }
}
