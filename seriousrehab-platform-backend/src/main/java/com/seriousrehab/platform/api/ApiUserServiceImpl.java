package com.seriousrehab.platform.api;

import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.repository.UserRepository;
import com.seriousrehab.platform.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
class ApiUserServiceImpl implements ApiUserService {

    private final UserService userService;
    private final UserRepository userRepository;

    @Override
    public ApiUser getApiUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null) {
            return null;
        }
        Authentication authentication = context.getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();
        if (principal instanceof ApiUser apiUser) {
            return apiUser;
        }
        return null;
    }

    @Override
    public User getUser() {
        ApiUser apiUser = getApiUser();
        return userService.findUserByUsername(apiUser.getUsername())
//                .orElse(userRepository.save(User.builder().username("michael.raimer@gmail.com").keycloakId("c6607fc5-614c-4ec5-8801-7bf1e55a8734").build()));
                .orElseThrow(UserNotFoundException::new);
    }
}
