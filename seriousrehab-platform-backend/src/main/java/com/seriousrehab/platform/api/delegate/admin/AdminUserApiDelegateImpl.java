package com.seriousrehab.platform.api.delegate.admin;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.UsersApiDelegate;
import com.seriousrehab.platform.api.mapper.UserMapper;
import com.seriousrehab.platform.api.model.KeycloakUserDto;
import com.seriousrehab.platform.api.model.UserDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.spi.NotImplementedYetException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminUserApiDelegateImpl implements UsersApiDelegate {

    private final UserService userService;
    private final ApiUserService apiUserService;
    private final UserMapper userMapper;

    //https://stackoverflow.com/questions/43222769/how-to-create-keycloak-client-role-programmatically-and-assign-to-user
    @Override
    public ResponseEntity<UserDto> createUser(UserDto userDto) {
        ApiUser currentUser = apiUserService.getApiUser();
        userDto.setTherapistId(null);
        UserDto user = userMapper.toDto(userService.createUser(userMapper.fromDto(userDto)));
        return ResponseEntity.ok(user);

        //TODO: listen to keycloak events
//        https://github.com/keycloak/keycloak-quickstarts/tree/latest/event-listener-sysout

//        if (response.getStatus() == HttpStatus.CREATED.value()) {
//            log.info(response.readEntity(String.class));
//            //TODO: create backend user
////                keycloak.realm("master").users().
//        } else {
//            log.error(response.readEntity(String.class));
//            throw new UserCreationException();
//        }
    }

    @Override
    public ResponseEntity<Void> deleteUser(String id) {
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserDto> getUserById(String id) {
        UserDto userDto = userService.findUserById(id)
                .map(userMapper::toDto)
                .orElseThrow(UserNotFoundException::new);
        return ResponseEntity.ok(userDto);
    }

    @Override
    public ResponseEntity<UserDto> getUserByUsername() {
        throw new NotImplementedYetException();
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userService.findAllUsers()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList()));
    }

    @Override
    public ResponseEntity<UserDto> updateUser(String id, UserDto userDto) {
        User user = userMapper.fromDto(userDto);
        user.setId(id);
        //TODO: only set specific fields of user?
        return ResponseEntity.ok(userMapper.toDto(userService.updateUser(user)));

    }

    @Override
    public ResponseEntity<Boolean> handleKeycloakRegistrationEvent(KeycloakUserDto keycloakUserDto) {
        throw new UnauthorizedMethodCallException();
    }
}
