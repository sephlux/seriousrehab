package com.seriousrehab.platform.api.mapper;

import org.springframework.stereotype.Component;

@Component
public interface Mapper<T, D> {

    D toDto(T user);

    T fromDto(D userDto);
}
