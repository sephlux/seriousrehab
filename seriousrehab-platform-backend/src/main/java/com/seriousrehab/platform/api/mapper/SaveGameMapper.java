package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.SaveGameDto;
import com.seriousrehab.platform.model.SaveGame;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;

@Component
public class SaveGameMapper implements Mapper<SaveGame, SaveGameDto> {
    @Override
    public SaveGameDto toDto(SaveGame game) {
        return new SaveGameDto()
                .id(game.getId())
                .createdAt(game.getCreatedAt().atOffset(ZoneOffset.UTC))
                .gameId(game.getGameId())
                .unityId(game.getUnityId())
                .payload(game.getPayload());
    }

    @Override
    public SaveGame fromDto(SaveGameDto gameDto) {
        return SaveGame.builder()
                .gameId(gameDto.getGameId())
                .payload(gameDto.getPayload())
                .build();
    }

    public SaveGame fromDto(SaveGameDto gameDto, String userId) {
        SaveGame game = fromDto(gameDto);
        game.setUserId(userId);
        return game;
    }
}
