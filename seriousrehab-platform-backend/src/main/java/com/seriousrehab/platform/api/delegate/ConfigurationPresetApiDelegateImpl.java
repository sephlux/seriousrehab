package com.seriousrehab.platform.api.delegate;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.ConfigurationPresetApiDelegate;
import com.seriousrehab.platform.api.delegate.admin.AdminConfigurationPresetApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.therapist.TherapistConfigurationPresetApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.user.UserConfigurationPresetApiDelegateImpl;
import com.seriousrehab.platform.api.model.ConfigurationPresetDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class ConfigurationPresetApiDelegateImpl implements ConfigurationPresetApiDelegate {

    private final ApiUserService apiUserService;

    private final List<ConfigurationPresetApiDelegate> configurationPresetApiDelegates;

    private ConfigurationPresetApiDelegate getDelegate() {
        ApiUser authenticatedUser = apiUserService.getApiUser();
        for (ConfigurationPresetApiDelegate configurationPresetApiDelegate : configurationPresetApiDelegates) {
            if (authenticatedUser.isAdmin()) {
                if (configurationPresetApiDelegate instanceof AdminConfigurationPresetApiDelegateImpl) {
                    return configurationPresetApiDelegate;
                }
            } else if (authenticatedUser.isTherapist()) {
                if (configurationPresetApiDelegate instanceof TherapistConfigurationPresetApiDelegateImpl) {
                    return configurationPresetApiDelegate;
                }
            } else {
                if (configurationPresetApiDelegate instanceof UserConfigurationPresetApiDelegateImpl) {
                    return configurationPresetApiDelegate;
                }
            }
        }
        throw new IllegalStateException("missing delegate");
    }

    @Override
    public ResponseEntity<ConfigurationPresetDto> createOrUpdateConfigurationPreset(ConfigurationPresetDto configurationPresetDto) {
        return getDelegate().createOrUpdateConfigurationPreset(configurationPresetDto);
    }

    @Override
    public ResponseEntity<Void> deleteConfigurationPreset(String id) {
        return ConfigurationPresetApiDelegate.super.deleteConfigurationPreset(id);
    }

    @Override
    public ResponseEntity<List<ConfigurationPresetDto>> getAllConfigurationPresets(String gameId) {
        return getDelegate().getAllConfigurationPresets(gameId);
    }

    @Override
    public ResponseEntity<ConfigurationPresetDto> getConfigurationById(String id) {
        return getDelegate().getConfigurationById(id);
    }

//    @Override
//    public ResponseEntity<ConfigurationPresetDto> updateConfigurationPreset(String id, ConfigurationPresetDto configurationPresetDto) {
//        return ConfigurationPresetApiDelegate.super.updateConfigurationPreset(id, configurationPresetDto);
//    }
}
