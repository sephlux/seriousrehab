package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.GameDto;
import com.seriousrehab.platform.model.Game;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GameMapper implements Mapper<Game, GameDto> {

    private final ConfigurationParameterMapper configurationParameterMapper;

    @Override
    public GameDto toDto(Game game) {
        return new GameDto()
                .id(game.getId())
                .title(game.getTitle())
                .fileTitle(game.getFileTitle())
                .description(game.getDescription())
                .storageUrl(game.getStorageUrl())
                .angle(game.getAngle())
                .thumbnailUrl(game.getThumbnailUrl())
                .ratio(game.getRatio())
                .gameTags(game.getGameTags().stream().map(Enum::toString).toList())
                .configurationParameters(game.getConfigurationParameters().stream().map(configurationParameterMapper::toDto).toList());
    }

    @Override
    public Game fromDto(GameDto gameDto) {
        return Game.builder()
                .title(gameDto.getTitle())
                .fileTitle(gameDto.getFileTitle())
                .description(gameDto.getDescription())
                .storageUrl(gameDto.getStorageUrl())
                .angle(gameDto.getAngle())
                .thumbnailUrl(gameDto.getThumbnailUrl())
                .ratio(gameDto.getRatio())
                .gameTags(gameDto.getGameTags().stream().map(String::toUpperCase).map(Game.GameTag::valueOf).toList())
                .configurationParameters(gameDto.getConfigurationParameters().stream().map(configurationParameterMapper::fromDto).toList())
                .build();

    }
}
