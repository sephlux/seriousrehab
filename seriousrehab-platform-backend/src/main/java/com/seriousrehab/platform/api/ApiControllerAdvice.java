package com.seriousrehab.platform.api;

import com.seriousrehab.platform.api.model.ApiError;
import com.seriousrehab.platform.model.exeptions.ApiErrorCode;
import com.seriousrehab.platform.model.exeptions.Localizable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
class ApiControllerAdvice {

    private final MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(Exception e) {
        ApiError apiError = new ApiError();

        if (e instanceof Localizable localizable) {
//            Locale locale = apiUtilsService.getLocale(exchange);
            apiError.localizedMessage(localizable.getLocalizedMessage(new MessageSourceAccessor(messageSource), null));
        }
        if (e instanceof ApiErrorCode apiErrorCode) {
            apiError.code(apiErrorCode.getApiErrorCode());
        }

        if (!(e instanceof Localizable) && !(e instanceof ApiErrorCode)) {
            log.error("Unknown error occurred", e);
        } else {
            log.error(e.getClass().getName(), e);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
    }


//    //TODO: localize
//    @ExceptionHandler(ApiErrorException.class)
//    public HttpEntity<ApiError> handleApiErrorException(ApiErrorException e) {
//        log.error("ApiErrorException: ", e);
//        return ResponseEntity.status(e.getHttpStatus()).body(e.getApiError());
//    }
//
//    @ExceptionHandler(UserNotFoundException.class)
//    public HttpEntity<ApiError> handleUserNotFoundException(UserNotFoundException e) {
//        log.error("UserNotFoundException: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError().localizedMessage("Given user not found!"));
//    }
//
//    @ExceptionHandler(UserCreationException.class)
//    public HttpEntity<ApiError> handleUserCreationException(UserCreationException e) {
//        log.error("UserCreationException: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError().localizedMessage("New user couldn't be created!"));
//    }
//
//    @ExceptionHandler(GameNotFoundException.class)
//    public HttpEntity<ApiError> handleGameNotFoundException(GameNotFoundException e) {
//        log.error("GameNotFoundException: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError().localizedMessage("Given game not found!"));
//    }
//
//    @ExceptionHandler(KeycloakClientNotFoundException.class)
//    public HttpEntity<ApiError> handleKeycloakClientNotFoundException(KeycloakClientNotFoundException e) {
//        log.error("This exceptions should not happen!");
//        log.error("KeycloakClientNotFoundException: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError());
//    }
//
//    @ExceptionHandler(KeycloakRoleNotFoundException.class)
//    public HttpEntity<ApiError> handleKeycloakRoleNotFoundException(KeycloakRoleNotFoundException e) {
//        log.error("This exceptions should not happen!");
//        log.error("KeycloakRoleNotFoundException: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError());
//    }
//
//    @ExceptionHandler(Exception.class)
//    public HttpEntity<ApiError> handleGameNotFoundException(Exception e) {
//        log.error("Exception: ", e);
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiError().localizedMessage("Exception!"));
//    }
}
