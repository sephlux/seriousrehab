package com.seriousrehab.platform.api.delegate;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.SaveGamesApiDelegate;
import com.seriousrehab.platform.api.delegate.admin.AdminSaveGamesApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.therapist.TherapistSaveGamesApiDelegateImpl;
import com.seriousrehab.platform.api.delegate.user.UserSaveGamesApiDelegateImpl;
import com.seriousrehab.platform.api.mapper.GameSessionMapper;
import com.seriousrehab.platform.api.mapper.SaveGameMapper;
import com.seriousrehab.platform.api.model.GameSessionDto;
import com.seriousrehab.platform.api.model.SaveGameDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.service.SaveGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Slf4j
@Primary
@Service
@RequiredArgsConstructor
public class SaveGamesApiDelegateImpl implements SaveGamesApiDelegate {


    private final ApiUserService apiUserService;
    private final SaveGameService saveGameService;
    private final SaveGameMapper saveGameMapper;
    private final GameSessionMapper gameSessionMapper;

    private final List<SaveGamesApiDelegate> saveGamesApiDelegates;

    private SaveGamesApiDelegate getDelegate() {
        ApiUser authenticatedUser = apiUserService.getApiUser();
        for (SaveGamesApiDelegate saveGamesApiDelegate : saveGamesApiDelegates) {
            if (authenticatedUser.isAdmin()) {
                if (saveGamesApiDelegate instanceof AdminSaveGamesApiDelegateImpl) {
                    return saveGamesApiDelegate;
                }
            } else if (authenticatedUser.isTherapist()) {
                if (saveGamesApiDelegate instanceof TherapistSaveGamesApiDelegateImpl) {
                    return saveGamesApiDelegate;
                }
            } else {
                if (saveGamesApiDelegate instanceof UserSaveGamesApiDelegateImpl) {
                    return saveGamesApiDelegate;
                }
            }
        }
        throw new IllegalStateException("missing delegate");
    }

    @Override
    public ResponseEntity<List<GameSessionDto>> getAllGameSessions(String userId) {
        return getDelegate().getAllGameSessions(userId);
    }

    @Override
    public ResponseEntity<GameSessionDto> getGameSession(String id) {
        return getDelegate().getGameSession(id);
    }

    @Override
    public ResponseEntity<Void> syncSaveGames(SaveGameDto saveGameDto) {
        log.info("syncSaveGames {}", saveGameDto);
        User authenticatedUser = apiUserService.getUser();
        saveGameService.syncGames(saveGameMapper.fromDto(saveGameDto, authenticatedUser.getId()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updateGameSession(String id, GameSessionDto gameSessionDto) {
        log.info("updateGameSession {}", gameSessionDto);
        User authenticatedUser = apiUserService.getUser();
        //TODO: check permission - only update own sessions?
        saveGameService.updateGameSession(id, gameSessionMapper.fromDto(gameSessionDto));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<GameSessionDto> getGameSessionToday(String userId, String gameId) {
        User authenticatedUser = apiUserService.getUser();
        return ResponseEntity.ok(saveGameService.getAllGameSessionsByUserId(authenticatedUser.getId()).stream()
                .filter(gameSession -> gameSession.getCreatedAt().isAfter(LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIDNIGHT)))
                .filter(gameSession -> StringUtils.equals(gameSession.getGameId(), gameId))
                .findFirst()
                .map(gameSessionMapper::toDto)
                .orElse(null));
    }
}
