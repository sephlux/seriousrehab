package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.CommentDto;
import com.seriousrehab.platform.model.Comment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;

@Component
@RequiredArgsConstructor
public class CommentMapper implements Mapper<Comment, CommentDto> {

    @Override
    public CommentDto toDto(Comment comment) {
        return new CommentDto()
                .id(comment.getId())
                .targetId(comment.getTargetId())
                .text(comment.getText())
                .createdAt(comment.getCreatedAt().atOffset(ZoneOffset.UTC))
                .createdBy(comment.getCreatedBy());
    }

    @Override
    public Comment fromDto(CommentDto commentDto) {
        Comment comment = Comment.builder()
                .targetId(commentDto.getTargetId())
                .text(commentDto.getText())
                .build();
        comment.setCreatedBy(commentDto.getCreatedBy());
        return comment;
    }
}
