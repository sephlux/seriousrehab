package com.seriousrehab.platform.api.delegate.therapist;

import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.SaveGamesApiDelegate;
import com.seriousrehab.platform.api.mapper.GameSessionMapper;
import com.seriousrehab.platform.api.model.GameSessionDto;
import com.seriousrehab.platform.api.model.SaveGameDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.SaveGameNotFoundException;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.service.SaveGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TherapistSaveGamesApiDelegateImpl implements SaveGamesApiDelegate {

    private final ApiUserService apiUserService;
    private final SaveGameService saveGameService;
    private final GameSessionMapper gameSessionMapper;

    @Override
    public ResponseEntity<List<GameSessionDto>> getAllGameSessions(String userId) {
        log.info("getAllGameSessions {}", userId);
        User authenticatedUser = apiUserService.getUser();
        if (userId != null) {
            return ResponseEntity.ok(saveGameService.getAllGameSessionsByUserIdAndTherapistId(userId, authenticatedUser.getId())
                    .stream()
                    .map(gameSessionMapper::toDto)
                    .toList());
        }
        return ResponseEntity.ok(saveGameService.getAllGameSessionsFromTherapist(authenticatedUser.getId())
                .stream()
                .map(gameSessionMapper::toDto)
                .toList());
    }

    @Override
    public ResponseEntity<GameSessionDto> getGameSession(String id) {
        log.info("getGameSession {}", id);
        User authenticatedUser = apiUserService.getUser();
        return ResponseEntity.ok(gameSessionMapper.toDto(
                saveGameService.getGameSessionByTherapist(id, authenticatedUser.getId())
                        .orElseThrow(SaveGameNotFoundException::new))
        );
    }

    @Override
    public ResponseEntity<Void> syncSaveGames(SaveGameDto saveGameDto) {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<Void> updateGameSession(String id, GameSessionDto gameSessionDto) {
        throw new UnauthorizedMethodCallException();
    }

}
