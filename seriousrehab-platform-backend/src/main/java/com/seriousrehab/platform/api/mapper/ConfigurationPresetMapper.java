package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.ConfigurationPresetDto;
import com.seriousrehab.platform.model.ConfigurationPreset;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ConfigurationPresetMapper implements Mapper<ConfigurationPreset, ConfigurationPresetDto> {

    private final GameSettingMapper gameSettingMapper;

    @Override
    public ConfigurationPresetDto toDto(ConfigurationPreset configurationPreset) {
        return new ConfigurationPresetDto()
                .therapistId(configurationPreset.getTherapistId())
                .gameId(configurationPreset.getGameId())
                .name(configurationPreset.getName())
                ._configuration(gameSettingMapper.toDto(configurationPreset.getConfiguration()));
    }

    @Override
    public ConfigurationPreset fromDto(ConfigurationPresetDto configurationPresetDto) {
        return ConfigurationPreset.builder()
                .therapistId(configurationPresetDto.getTherapistId())
                .gameId(configurationPresetDto.getGameId())
                .name(configurationPresetDto.getName())
                .configuration(gameSettingMapper.fromDto(configurationPresetDto.getConfiguration()))
                .build();
    }

}
