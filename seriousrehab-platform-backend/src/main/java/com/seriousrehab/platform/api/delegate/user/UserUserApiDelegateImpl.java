package com.seriousrehab.platform.api.delegate.user;

import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.UsersApiDelegate;
import com.seriousrehab.platform.api.mapper.UserMapper;
import com.seriousrehab.platform.api.model.KeycloakUserDto;
import com.seriousrehab.platform.api.model.UserDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.UnauthorizedAccessException;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserUserApiDelegateImpl implements UsersApiDelegate {

    private final UserService userService;
    private final ApiUserService apiUserService;
    private final UserMapper userMapper;

    //https://stackoverflow.com/questions/43222769/how-to-create-keycloak-client-role-programmatically-and-assign-to-user
    @Override
    public ResponseEntity<UserDto> createUser(UserDto userDto) {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<Void> deleteUser(String id) {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<UserDto> getUserById(String id) {
        User user = apiUserService.getUser();
        UserDto userDto = userService.findUserById(user.getId())
                .map(userMapper::toDto)
                .orElseThrow(UserNotFoundException::new);
        return ResponseEntity.ok(userDto);
    }

    @Override
    public ResponseEntity<UserDto> getUserByUsername() {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        User user = apiUserService.getUser();
        return ResponseEntity.ok(List.of(userService.findUserById(user.getId())
                .map(userMapper::toDto)
                .orElseThrow(UserNotFoundException::new)));
    }

    @Override
    public ResponseEntity<UserDto> updateUser(String id, UserDto userDto) {
        //check permissions
        User authenticatedUser = apiUserService.getUser();
        if (!StringUtils.equals(authenticatedUser.getId(), id)) {
            //TODO: handle
            throw new UnauthorizedAccessException();
        }
        User user = userMapper.fromDto(userDto);
        user.setId(id);
        return ResponseEntity.ok(userMapper.toDto(userService.updateUser(user)));
    }

    @Override
    public ResponseEntity<Boolean> handleKeycloakRegistrationEvent(KeycloakUserDto keycloakUserDto) {
        throw new UnauthorizedMethodCallException();
    }
}


