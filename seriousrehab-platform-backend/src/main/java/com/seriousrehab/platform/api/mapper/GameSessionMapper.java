package com.seriousrehab.platform.api.mapper;


import com.seriousrehab.platform.api.model.GameSessionDto;
import com.seriousrehab.platform.model.Game;
import com.seriousrehab.platform.model.GameSession;
import com.seriousrehab.platform.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;

@Component
@RequiredArgsConstructor
public class GameSessionMapper implements Mapper<GameSession, GameSessionDto> {

    private final SaveGameMapper saveGameMapper;

    private final GameService gameService;

    @Override
    public GameSessionDto toDto(GameSession gameSession) {
        return new GameSessionDto()
                .id(gameSession.getId())
                .userId(gameSession.getUserId())
                .gameId(gameSession.getGameId())
                .gameName(gameService.findGameById(gameSession.getGameId()).map(Game::getTitle).orElse("undefined"))
                .pain(gameSession.getPain())
                .feeling(gameSession.getFeeling())
                .swelling(gameSession.getSwelling())
                .painComment(gameSession.getPainComment())
                .feelingComment(gameSession.getFeelingComment())
                .swellingComment(gameSession.getSwellingComment())
                .createdAt(gameSession.getCreatedAt().atOffset(ZoneOffset.UTC))
                .playedGames(gameSession.getSaveGames().stream().map(saveGameMapper::toDto).toList());
    }

    @Override
    public GameSession fromDto(GameSessionDto gameSessionDto) {
        return GameSession.builder()
                .userId(gameSessionDto.getUserId())
                .gameId(gameSessionDto.getGameId())
                .pain(gameSessionDto.getPain())
                .feeling(gameSessionDto.getFeeling())
                .swelling(gameSessionDto.getSwelling())
                .painComment(gameSessionDto.getPainComment())
                .feelingComment(gameSessionDto.getFeelingComment())
                .swellingComment(gameSessionDto.getSwellingComment())
                .saveGames(gameSessionDto.getPlayedGames().stream().map(saveGameMapper::fromDto).toList())
                .build();
    }
}
