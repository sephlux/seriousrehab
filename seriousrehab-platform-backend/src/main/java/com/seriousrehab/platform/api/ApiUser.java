package com.seriousrehab.platform.api;

import com.nimbusds.jwt.JWTClaimsSet;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.bson.json.JsonObject;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Data
public class ApiUser {

    private String username;
    private List<String> roles;

    public ApiUser(JWTClaimsSet claims) throws ParseException {
        this.username = claims.getStringClaim("preferred_username");
        this.roles = new JsonObject(claims.getJSONObjectClaim("resource_access").get("seriousrehab-app").toString())
                .toBsonDocument()
                .getArray("roles")
                .stream()
                .map(bsonValue -> bsonValue.asString().getValue())
                .collect(Collectors.toList());
    }

    public boolean isTherapist() {
        return roles.contains("THERAPIST");
    }

    public boolean isAdmin() {
        return roles.contains("ADMIN");
    }

}
