package com.seriousrehab.platform.api.mapper;

import com.seriousrehab.platform.api.model.GameSettingDto;
import com.seriousrehab.platform.model.User;
import org.springframework.stereotype.Component;

@Component
public class GameSettingMapper implements Mapper<User.GameSetting, GameSettingDto> {
    @Override
    public GameSettingDto toDto(User.GameSetting gameSetting) {
        return new GameSettingDto()
                .gameId(gameSetting.getGameId())
                .enabled(gameSetting.isEnabled())
                .settingsLocked(gameSetting.isSettingsLocked())
                .maxGameTime(gameSetting.getMaxGameTime())
                .settings(gameSetting.getSettings());
    }

    @Override
    public User.GameSetting fromDto(GameSettingDto gameSettingDto) {
        User.GameSetting gameSetting = new User.GameSetting();
        gameSetting.setGameId(gameSettingDto.getGameId());
        gameSetting.setEnabled(gameSettingDto.getEnabled());
        gameSetting.setSettingsLocked(gameSettingDto.getSettingsLocked());
        gameSetting.setMaxGameTime(gameSettingDto.getMaxGameTime());
        gameSetting.setSettings(gameSettingDto.getSettings());
        return gameSetting;
    }
}
