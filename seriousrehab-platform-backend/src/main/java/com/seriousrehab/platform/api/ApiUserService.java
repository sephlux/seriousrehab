package com.seriousrehab.platform.api;

import com.seriousrehab.platform.model.User;

public interface ApiUserService {

    ApiUser getApiUser();

    User getUser();

}
