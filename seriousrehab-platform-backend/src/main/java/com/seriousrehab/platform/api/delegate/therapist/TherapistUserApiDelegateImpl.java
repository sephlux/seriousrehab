package com.seriousrehab.platform.api.delegate.therapist;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.UsersApiDelegate;
import com.seriousrehab.platform.api.mapper.UserMapper;
import com.seriousrehab.platform.api.model.KeycloakUserDto;
import com.seriousrehab.platform.api.model.UserDto;
import com.seriousrehab.platform.model.User;
import com.seriousrehab.platform.model.exeptions.UnauthorizedAccessException;
import com.seriousrehab.platform.model.exeptions.UnauthorizedMethodCallException;
import com.seriousrehab.platform.model.exeptions.UserNotFoundException;
import com.seriousrehab.platform.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TherapistUserApiDelegateImpl implements UsersApiDelegate {

    private final UserService userService;
    private final ApiUserService apiUserService;
    private final UserMapper userMapper;

    //https://stackoverflow.com/questions/43222769/how-to-create-keycloak-client-role-programmatically-and-assign-to-user
    @Override
    public ResponseEntity<UserDto> createUser(UserDto userDto) {
        userDto.setTherapistId(apiUserService.getUser().getId());
        UserDto user = userMapper.toDto(userService.createUser(userMapper.fromDto(userDto)));
        return ResponseEntity.ok(user);
    }

    @Override
    public ResponseEntity<Void> deleteUser(String id) {
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserDto> getUserById(String id) {
        User user = apiUserService.getUser();
        if (StringUtils.equals(user.getId(), id)) {
            return ResponseEntity.ok(userMapper.toDto(user));
        }
        return ResponseEntity.ok(userService.findUserByIdAndTherapistId(id, user.getId())
                .map(userMapper::toDto)
                .orElseThrow(UserNotFoundException::new));
    }

    @Override
    public ResponseEntity<UserDto> getUserByUsername() {
        throw new UnauthorizedMethodCallException();
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        ApiUser currentUser = apiUserService.getApiUser();
        //TODO: check if user is allowed to retrieve data of this user
        return ResponseEntity.ok(userService.findAllUsersByTherapist(apiUserService.getUser().getId())
                .stream()
                .map(userMapper::toDto)
                .toList());
    }

    @Override
    public ResponseEntity<UserDto> updateUser(String id, UserDto userDto) {
        User authenticatedUser = apiUserService.getUser();
        User user = userMapper.fromDto(userDto);
        user.setId(id);
        User target = authenticatedUser;
        if (!StringUtils.equals(user.getId(), id)) {
            target = userService.findUserByIdAndTherapistId(id, authenticatedUser.getId())
                    .orElseThrow(UserNotFoundException::new);
        }
        if (!StringUtils.equals(target.getTherapistId(), authenticatedUser.getId()) && !StringUtils.equals(target.getId(), authenticatedUser.getId())) {
            //TODO: handle
            throw new UnauthorizedAccessException();
        }

        //TODO: only set specific fields of user?
        return ResponseEntity.ok(userMapper.toDto(userService.updateUser(user)));
    }

    @Override
    public ResponseEntity<Boolean> handleKeycloakRegistrationEvent(KeycloakUserDto keycloakUserDto) {
        throw new UnauthorizedMethodCallException();
    }
}

