package com.seriousrehab.platform.api.delegate;

import com.seriousrehab.platform.api.ApiUser;
import com.seriousrehab.platform.api.ApiUserService;
import com.seriousrehab.platform.api.CommentsApiDelegate;
import com.seriousrehab.platform.api.mapper.CommentMapper;
import com.seriousrehab.platform.api.model.CommentDto;
import com.seriousrehab.platform.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommentApiDelegateImpl implements CommentsApiDelegate {

    private final ApiUserService apiUserService;
    private final CommentService commentService;
    private final CommentMapper commentMapper;

    //TODO: try catch??

    @Override
    public ResponseEntity<CommentDto> createComment(CommentDto commentDto) {
        log.info("createComment {}", commentDto);
        ApiUser currentUser = apiUserService.getApiUser();
        commentDto.setCreatedBy(currentUser.getUsername());
        return ResponseEntity.ok(commentMapper.toDto(commentService.createComment(commentMapper.fromDto(commentDto))));
    }

    @Override
    public ResponseEntity<List<CommentDto>> getAllComments(String targetId) {
        log.info("getAllComments {}", targetId);
        return ResponseEntity.ok(commentService.findAllComments(targetId)
                .stream()
                .map(commentMapper::toDto)
                .toList());
    }
}
