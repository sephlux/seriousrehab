package com.seriousrehab.platform;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;


@Configuration
public class SeriousRehabBackendWebConfiguration extends WebSecurityConfigurerAdapter {

    //    https://www.baeldung.com/spring-security-oauth-resource-server
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .csrf().disable()
                .cors().and()
                .authorizeRequests().antMatchers("/api/users/handle_keycloak_registration").permitAll()//TODO: secure
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and().oauth2ResourceServer().jwt();

        http.addFilterBefore(new RtaJwtAuthenticationProcessingFilter(), BasicAuthenticationFilter.class);

    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(List.of("*"));
        configuration.setAllowedMethods(List.of("*"));
        configuration.setAllowedHeaders(List.of("*"));
        configuration.setAllowedOriginPatterns(List.of("*"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}


//                .authorizeRequests().anyRequest().permitAll();

//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                .requestMatchers()
//                .antMatchers("/api/v1/**")
//                .and()
//                .cors().and()
//                .authorizeRequests()
//                .antMatchers("/api/v1/app/**").permitAll()
//                .antMatchers("/api/v1/pro/**").hasRole("API_PRO")
//                .anyRequest().hasRole("API_USER");

//        http.addFilterBefore(new RtaJwtAuthenticationProcessingFilter(), BasicAuthenticationFilter.class);
// @formatter:on


//http.cors()
//        .and()
//        .authorizeRequests()
//        .antMatchers(HttpMethod.GET, "/user/info", "/api/foos/**")
//        .hasAuthority("SCOPE_read")
//        .antMatchers(HttpMethod.POST, "/api/foos")
//        .hasAuthority("SCOPE_write")
//        .anyRequest()
//        .authenticated()
//        .and()
//        .oauth2ResourceServer()
//        .jwt();
//        }