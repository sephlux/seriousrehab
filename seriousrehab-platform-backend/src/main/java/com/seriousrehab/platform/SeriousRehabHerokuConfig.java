package com.seriousrehab.platform;

import com.seriousrehab.slack.SlackEventHandler;
import com.seriousrehab.slack.SlackEventHandlerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;


@Configuration
@Profile("heroku")
public class SeriousRehabHerokuConfig {

    @Bean
    public SlackEventHandler slackMessageService(Environment env) {
        return new SlackEventHandlerImpl(env);
    }

}
