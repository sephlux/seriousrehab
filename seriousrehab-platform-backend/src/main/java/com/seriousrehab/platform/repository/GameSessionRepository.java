package com.seriousrehab.platform.repository;

import com.seriousrehab.platform.model.GameSession;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface GameSessionRepository extends MongoRepository<GameSession, String> {

    List<GameSession> findAllByUserIdAndTherapistIdOrderByCreatedAtDesc(String userId, String therapistId);

    List<GameSession> findAllByUserIdOrderByCreatedAtDesc(String userId);

    List<GameSession> findAllByUserIdAndGameIdAndCreatedAtBetween(String userId, String gameId, LocalDateTime from, LocalDateTime to);

    List<GameSession> findAllByOrderByCreatedAtDesc();

    List<GameSession> findAllByTherapistIdOrderByCreatedAtDesc(String id);

    Optional<GameSession> findByIdAndUserId(String id, String userId);

    Optional<GameSession> findByIdAndTherapistId(String id, String therapistId);
}
