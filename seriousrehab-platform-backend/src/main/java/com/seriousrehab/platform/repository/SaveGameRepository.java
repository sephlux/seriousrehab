package com.seriousrehab.platform.repository;

import com.seriousrehab.platform.model.SaveGame;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SaveGameRepository extends MongoRepository<SaveGame, String> {

    List<SaveGame> findAllByUserIdOrderByCreatedAtDesc(String userId);

    List<SaveGame> findAllByUserIdAndGameIdAndCreatedAtBetweenOrderByCreatedAtDesc(String userId, String gameId, LocalDateTime from, LocalDateTime to);

    Optional<SaveGame> findByUnityId(String unityId);


}
