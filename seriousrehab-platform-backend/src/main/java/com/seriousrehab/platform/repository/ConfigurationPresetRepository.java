package com.seriousrehab.platform.repository;

import com.seriousrehab.platform.model.ConfigurationPreset;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigurationPresetRepository extends MongoRepository<ConfigurationPreset, String> {

    Optional<ConfigurationPreset> findByIdAndTherapistId(String id, String therapistId);

    Optional<ConfigurationPreset> findByNameAndTherapistId(String name, String therapistId);

    List<ConfigurationPreset> findAllByTherapistIdAndGameId(String therapistId, String gameId);
}
