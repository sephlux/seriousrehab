package com.seriousrehab.platform.repository;

import com.seriousrehab.platform.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUsername(String username);

    List<User> findAllByTherapistIdOrderByCreatedAtDesc(String therapistId);

    List<User> findAllByOrderByCreatedAtDesc();

    Optional<User> findByIdAndTherapistId(String userId, String therapistId);
}
