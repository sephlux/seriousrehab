package com.seriousrehab.platform.repository;

import com.seriousrehab.platform.model.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends MongoRepository<Game, String> {

    List<Game> findAllByGameTypesContaining(Game.GameType gameType);

    List<Game> findAllByOrderByCreatedAt();

}
