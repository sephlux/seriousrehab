package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@Document(collection = "Game")
public class Game extends BaseDocument {

    private String title;
    private String fileTitle;
    private String description;
    private String storageUrl;
    private String thumbnailUrl;

    private String angle;
    private String ratio;
    private List<GameTag> gameTags;
    private List<GameType> gameTypes;
    private List<ConfigurationParameter> configurationParameters;


    public enum GameTag {
        ANKLE("Ankle"),
        DORSALEXTENSION("Dorsalextension"),
        PLANTARFLEXION("Plantarflexion"),
        DIFFERENT_LEVELS("Different Levels"),
        HIGHSCORE("Highscore");

        private String tag;

        GameTag(String tag) {
            this.tag = tag;
        }

    }

    public enum GameType {
        ANKLE
    }

    @Data
    public static class ConfigurationParameter {
        private boolean configurable;
        private String fieldName;
        private String title;
        private ParameterType type;
        private BigDecimal min;
        private BigDecimal max;
        private BigDecimal value;
        private BigDecimal defaultValue;

    }


    public enum ParameterType {
        VALUE("Value"),
        RANGE("Range");

        private String type;

        ParameterType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
}
