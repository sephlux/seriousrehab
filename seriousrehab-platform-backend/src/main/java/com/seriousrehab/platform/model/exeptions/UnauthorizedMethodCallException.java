package com.seriousrehab.platform.model.exeptions;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "UNAUTHORIZED_METHOD")
public class UnauthorizedMethodCallException extends RuntimeException implements Localizable, ApiErrorCode {
    @Override
    public String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale) {
        return "Trying to call method unauthorized!";
//        return messageSource.getMessage("API_ERROR_REDEEM_VALIDATION_FAILED", locale);
    }

    @Override
    public String getApiErrorCode() {
        return "UNAUTHORIZED_METHOD";
    }
}

