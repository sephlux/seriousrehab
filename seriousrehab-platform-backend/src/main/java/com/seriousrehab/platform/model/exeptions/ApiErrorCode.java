package com.seriousrehab.platform.model.exeptions;

public interface ApiErrorCode {
    String getApiErrorCode();
}
