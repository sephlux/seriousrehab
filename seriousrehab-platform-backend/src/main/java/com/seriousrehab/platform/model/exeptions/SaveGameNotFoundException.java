package com.seriousrehab.platform.model.exeptions;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "SAVE_GAME_NOT_FOUND")
public class SaveGameNotFoundException extends RuntimeException implements Localizable, ApiErrorCode {
    @Override
    public String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale) {
        return "Given save game not found!";
//        return messageSource.getMessage("API_ERROR_REDEEM_VALIDATION_FAILED", locale);
    }

    @Override
    public String getApiErrorCode() {
        return "SAVE_GAME_NOT_FOUND";
    }
}
