package com.seriousrehab.platform.model.exeptions;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "EMAIL_ALREADY_REGISTERED")
public class UserCreationException extends RuntimeException implements Localizable, ApiErrorCode {

    //TODO: change the reasons of all exceptions

    @Override
    public String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale) {
        return "New user couldn't be created!";
//        return messageSource.getMessage("API_ERROR_REDEEM_VALIDATION_FAILED", locale);
    }

    @Override
    public String getApiErrorCode() {
        return "EMAIL_ALREADY_REGISTERED";
    }
}
