package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "SaveGame")
public class SaveGame extends BaseDocument {

    private String userId;
    private String gameId;
    private String unityId;
    private String payload;

}
