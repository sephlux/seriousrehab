package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "ConfigurationPreset")
public class ConfigurationPreset extends BaseDocument {

    private String therapistId;
    private String gameId;
    private String name;
    private User.GameSetting configuration;

}
