package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "Comment")
public class Comment extends BaseDocument {

    String targetId;
    String text;

}
