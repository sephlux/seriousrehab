package com.seriousrehab.platform.model.exeptions;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "GAME_SESSION_NOT_FOUND")
public class GameSessionNotFoundException extends RuntimeException implements Localizable, ApiErrorCode {
    @Override
    public String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale) {
        return "Given game session not found!";
//        return messageSource.getMessage("API_ERROR_REDEEM_VALIDATION_FAILED", locale);
    }

    @Override
    public String getApiErrorCode() {
        return "GAME_SESSION_NOT_FOUND";
    }
}
