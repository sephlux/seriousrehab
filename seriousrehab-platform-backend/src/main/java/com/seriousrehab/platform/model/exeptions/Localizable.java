package com.seriousrehab.platform.model.exeptions;

import org.springframework.context.support.MessageSourceAccessor;

import java.util.Locale;

public interface Localizable {

    String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale);
}
