package com.seriousrehab.platform.model.exeptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Locale;

@Slf4j
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "EMAIL_ALREADY_REGISTERED")
public class KeycloakRoleNotFoundException extends RuntimeException implements Localizable, ApiErrorCode {
    @Override
    public String getLocalizedMessage(MessageSourceAccessor messageSource, Locale locale) {
        log.error("This exceptions should not happen!");
        return "";
//        return messageSource.getMessage("API_ERROR_REDEEM_VALIDATION_FAILED", locale);
    }

    @Override
    public String getApiErrorCode() {
        return "EMAIL_ALREADY_REGISTERED";
    }
}
