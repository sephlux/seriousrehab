package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@Builder
@Document(collection = "User")
public class User extends BaseDocument {

    String keycloakId;
    String therapistId;
    String username;
    String firstName;
    String lastName;
    boolean deleted;
    List<GameSetting> gameSettings = new ArrayList<>();

    @Data
    public static class GameSetting {
        String gameId;
        boolean enabled;
        boolean settingsLocked;
        int maxGameTime;
        Map<String, String> settings;
    }

    public boolean isTherapist() {
        return StringUtils.isEmpty(therapistId);
    }

}
