package com.seriousrehab.platform.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Transient;
import java.util.List;

@Data
@Builder
@Document(collection = "GameSession")
public class GameSession extends BaseDocument {

    @Transient
    List<SaveGame> saveGames;

    @Transient
    List<Comment> comments;

    String userId;
    String gameId;
    String therapistId;
    int pain;
    int swelling;
    int feeling;
    String painComment;
    String swellingComment;
    String feelingComment;


}
