package com.seriousrehab.platform;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "seriousrehab.keycloak")
public class SeriousRehabKeycloakProperties {
    private String url;
    private String realm;
    private String username;
    private String password;
    private String clientid;
}
