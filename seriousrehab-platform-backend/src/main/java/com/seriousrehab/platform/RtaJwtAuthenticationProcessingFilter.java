package com.seriousrehab.platform;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.seriousrehab.platform.api.ApiUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

class RtaJwtAuthenticationProcessingFilter extends GenericFilterBean {


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        try {
            final HttpServletRequest request = (HttpServletRequest) servletRequest;
            String authToken = request.getHeader(HttpHeaders.AUTHORIZATION);
            if (StringUtils.startsWithIgnoreCase(authToken, "Bearer")) {
                String accessToken = StringUtils.substringAfter(authToken, "Bearer ");


                JWT jwt = JWTParser.parse(accessToken);
                JWTClaimsSet claims = jwt.getJWTClaimsSet();


                ApiUser apiUser = new ApiUser(claims);
                final Collection<GrantedAuthority> authorities;
                if (apiUser.isAdmin()) {
                    authorities = List.of(new SimpleGrantedAuthority("ROLE_API_ADMIN"));
                } else if (apiUser.isTherapist()) {
                    authorities = List.of(new SimpleGrantedAuthority("ROLE_API_THERAPIST"));
                } else {
                    authorities = List.of(new SimpleGrantedAuthority("ROLE_API_USER"));
                }
                PreAuthenticatedAuthenticationToken authenticationToken = new PreAuthenticatedAuthenticationToken(apiUser, null, authorities);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (IOException | ServletException | ParseException e) {
            throw new RuntimeException(e);
        }
    }
}

