package com.seriousrehab.slack


object SlackMessageBuilder {

    fun message(init: SlackMessage.() -> Unit): SlackMessage {
        val message = SlackMessage()
        message.init()
        return message
    }

//    fun SlackMessage.attachment(init: SlackAttachment.() -> Unit): SlackAttachment {
//        val attachment = SlackAttachment()
//        attachment.init()
//        this.attachments.add(attachment)
//        return attachment
//    }

//    fun SlackAttachment.field(init: SlackField.() -> Unit): SlackField {
//        val field = SlackField()
//        field.init()
//        this.fields.add(field)
//        return field
//    }
}
