package com.seriousrehab.slack

data class SimpleNotificationEvent(
        val text: String? = null
)
