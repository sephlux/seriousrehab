package com.seriousrehab.slack

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import ch.qos.logback.core.Layout
import com.seriousrehab.slack.SlackMessageBuilder.message
import org.apache.commons.lang3.StringUtils
import org.springframework.web.client.RestTemplate


class SlackAppender : AppenderBase<ILoggingEvent>() {

    private val restTemplate = RestTemplate()

    var channel = SlackConstants.seriousRehabStacktraceProduction

    lateinit var layout: Layout<ILoggingEvent>

    override fun append(loggingEvent: ILoggingEvent) {
        val message = layout.doLayout(loggingEvent)
        restTemplate.postForObject(SlackConstants.webHookUrl, message {
            chat_id = this@SlackAppender.channel
            text = StringUtils.truncate(message, 4096)
        }, String::class.java)
    }
}
