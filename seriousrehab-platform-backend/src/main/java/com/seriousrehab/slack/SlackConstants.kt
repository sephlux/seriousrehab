package com.seriousrehab.slack


object SlackConstants {
    const val webHookUrl = "https://api.telegram.org/bot5937806159:AAF8wkPjb5cjCCoibRtM1EfdInibRX1P9Q4/sendMessage"
    const val seriousRehabInfoProduction = "-1001845130035"
    const val seriousRehabInfoStaging = "-1001864276916"
    const val seriousRehabStacktraceProduction = "-1001867520621"
    const val seriousRehabStacktraceStaging = "-1001627457525"
}
