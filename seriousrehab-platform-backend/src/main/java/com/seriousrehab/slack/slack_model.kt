package com.seriousrehab.slack


data class SlackMessage(var chat_id: String? = null,
                        var text: String? = null)

//data class SlackAttachment(var text: String? = null,
//                           var fallback: String? = null,
//                           var pretext: String? = null,
//                           var color: String? = null,
//                           var fields: MutableList<SlackField> = mutableListOf())
//
//data class SlackField(var title: String? = null,
//                      var value: String? = null,
//                      @JsonProperty("short") val shortValue: Boolean = false)
