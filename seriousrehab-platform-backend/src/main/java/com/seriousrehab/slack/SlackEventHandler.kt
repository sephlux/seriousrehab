package com.seriousrehab.slack


import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async

interface SlackEventHandler {
    @Async
    @EventListener
    fun onSimpleNotificationEvent(event: SimpleNotificationEvent)
}

