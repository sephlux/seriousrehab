package com.seriousrehab.slack

import org.apache.commons.lang3.StringUtils
import org.springframework.core.env.Environment
import org.springframework.web.client.RestTemplate

open class SlackEventHandlerImpl(
        val env: Environment
) : SlackEventHandler {

    companion object {
        private val restTemplate = RestTemplate()
    }

    override fun onSimpleNotificationEvent(event: SimpleNotificationEvent) {
        restTemplate.postForObject(SlackConstants.webHookUrl, SlackMessageBuilder.message {
            chat_id = if (env.acceptsProfiles("prod")) SlackConstants.seriousRehabInfoProduction else SlackConstants.seriousRehabInfoStaging
            text = StringUtils.truncate(event.text, 4096)
        }, String::class.java)
    }
}
