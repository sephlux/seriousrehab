package com.seriousrehab.platform.app.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import java.util.Locale;
import java.util.TimeZone;

//@EnableMongoDistributedLock
//@EntityScan(basePackageClasses = {SeriousRehabApplication.class})
@SpringBootApplication(scanBasePackages = "com.seriousrehab.platform")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableMongoRepositories
@EnableMongoAuditing
@EnableScheduling
@EnableAsync
@Slf4j
public class SeriousRehabApplication {

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Locale.setDefault(Locale.ENGLISH);
        SpringApplication.run(SeriousRehabApplication.class);
    }
}
